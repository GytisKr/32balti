<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Doctor extends Model
{
    use Translatable;
    protected $translatable = ['title','position','excerpt','body','slug','meta_desc'];
    
    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }
    public function serviceCategories()
    {
        return $this->belongsToMany(Service::class);
    }
}
