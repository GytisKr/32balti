<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Perk extends Model
{
    use Translatable;
    protected $translatable = ['title','meta_desc'];
}
