<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServicesController extends Controller
{
    public function index()
    {
        $services = Service::withTranslation(app()->getLocale())->where('status', 1)->orderBy('order', 'ASC')->get();

        return view('services.index', compact('services'));
    }

    public function show($locale, $slug)
    {
        $service = Service::findBySlug($slug);

        return view('services.show', compact('service'));
    }
}
