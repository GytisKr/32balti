<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;

class PricesController extends Controller
{
    public function index()
    {
        $prices = Price::withTranslation(app()->getLocale())->where('status', 1)->orderBy('order', 'ASC')->get();

        return view('pages.prices', compact('prices'));
    }
}
