<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perk;

class PerksController extends Controller
{
    public function index()
    {
        $perks = Perk::withTranslation(app()->getLocale())->where('status', 1)->orderBy('order', 'ASC')->get();

        return view('pages.perks', compact('perks'));
    }
}
