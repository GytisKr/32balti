<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use App\Slider;
use App\Perk;
use App\Doctor;
use App\Service;


class HomeController extends Controller
{
    public function redirect()
    {
        return redirect(app()->getLocale());    
    }

    public function index()
    {
        $sliders = Slider::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->get();
        $perks = Perk::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->paginate(8);
        $doctors = Doctor::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->paginate(6);
        $services = Service::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->paginate(8);
        $highlights = Service::where('highlighted', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->get();

        return view('welcome', compact('sliders','perks','doctors','services','highlights'));
    }
}