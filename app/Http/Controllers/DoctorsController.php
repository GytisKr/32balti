<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;

class DoctorsController extends Controller
{
    public function index()
    {
        $doctors = Doctor::withTranslation(app()->getLocale())->where('status', 1)->orderBy('order', 'ASC')->get();

        return view('doctors.index', compact('doctors'));
    }

    public function show($locale, $slug)
    {
        $doctor = Doctor::findBySlug($slug);

        return view('doctors.show', compact('doctor'));
    }
}