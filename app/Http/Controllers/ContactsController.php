<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use GuzzleHttp\Client;

class ContactsController extends Controller
{
    public function index()
    {
        return view('pages.contacts');
    }

    //REGISTRACIJA INTERNETU

    public function register(Request $request)
    {
        $name = $request->input('name');
        $phone = $request->input('phone');
        $time = $request->input('time');
        $purpose = $request->input('purpose');
        $service = $request->input('service');
        $fmessage = $request->input('fmessage');

        Mail::send('emails.registration',
            [
                'name' => $name,
                'phone' => $phone,
                'time' => $time,
                'purpose' => $purpose,
                'service' => $service,
                'fmessage' => $fmessage
            ],
            function ($message)
            {
                $message->from('admin@32balti.lt');
                $message->to('gytis@expertmedia.lt');
                $message->subject('Registracija internetu - 32balti.lt');
            }
        );

        $this->reg_smartsheet($request->all());

        return response()->json(['status' => true]);

    }

    public function reg_smartsheet($data)
    {
        $name = $data['name'];
        $phone = $data['phone'];
        $time = $data['time'];
        $purpose = $data['purpose'];
        $service = $data['service'];
        $fmessage = $data['fmessage'];
        $sheetId = $data['sheet-id'];

        $apiToken = 'ow6cw8qz3kkq3bacjxpvgfjzem';

        if (!empty($sheetId)) {
            $params = [
                'toTop' => true,
                'cells' => [
                    [
                        'columnId' => 6187990325192580,
                        'value' => $name
                    ],
                    [
                        'columnId' => 3936190511507332,
                        'value' => $phone
                    ],
                    [
                        'columnId' => 8439790138877828,
                        'value' => $time
                    ],
                    [
                        'columnId' => 1121440744400772,
                        'value' => $purpose
                    ],
                    [
                        'columnId' => 5625040371771268,
                        'value' => $service
                    ],
                    [
                        'columnId' => 3373240558086020,
                        'value' => $fmessage
                    ]
                ]
            ];

            $info = json_encode($params);
            
            $client = new Client([
                'base_uri' => 'https://api.smartsheet.com',
                'timeout' => 2.0,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$apiToken
                ]
            ]);

            $response = $client->post('/2.0/sheets/'.$sheetId.'/rows', [
                'body' => $info
            ]);


            if ($response->getStatusCode() == 200) {
                return response()->json(['status' => true]);
            } else {
                return $response->getStatusCode();
            }

        } else {
            return response()->json(['error' => 'Sheet ID is empty']);
        }
    }

    //KLAUSIMAS GYDYTOJUI

    public function contactus(Request $request)
    {
        $ask_name = $request->input('ask_name');
        $ask_email = $request->input('ask_email');
        $ask_phone = $request->input('ask_phone');
        $ask_doctor = $request->input('ask_doctor');
        $ask_comment = $request->input('ask_comment');

        Mail::send('emails.contactus',
            [
                'ask_name' => $ask_name,
                'ask_email' => $ask_mail,
                'ask_phone' => $ask_phone,
                'ask_doctor' => $ask_doctor,
                'ask_comment' => $ask_comment,
            ],
            function ($message)
            {
                $message->from('admin@32balti.lt');
                $message->to('gytis@expertmedia.lt');
                $message->subject('Klausimas gydytojui - 32balti.lt');
            }
        );

        $this->ask_smartsheet($request->all());

        return response()->json(['status' => true]);

    }

    public function ask_smartsheet($data)
    {
        $ask_name = $data['ask_name'];
        $ask_email = $data['ask_email'];
        $ask_phone = $data['ask_phone'];
        $ask_doctor = $data['ask_doctor'];
        $ask_comment = $data['ask_comment'];
        $sheetId = $data['sheet-id'];

        $apiToken = 'ow6cw8qz3kkq3bacjxpvgfjzem';

        if (!empty($sheetId)) {
            $params = [
                'toTop' => true,
                'cells' => [
                    [
                        'columnId' => 6904534617352068,
                        'value' => $ask_doctor
                    ],
                    [
                        'columnId' => 1275035083138948,
                        'value' => $ask_name
                    ],
                    [
                        'columnId' => 5778634710509444,
                        'value' => $ask_email
                    ],
                    [
                        'columnId' => 3526834896824196,
                        'value' => $ask_phone
                    ],
                    [
                        'columnId' => 8030434524194692,
                        'value' => $ask_comment
                    ],
                ]
            ];

            $info = json_encode($params);
            
            $client = new Client([
                'base_uri' => 'https://api.smartsheet.com',
                'timeout' => 2.0,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$apiToken
                ]
            ]);

            $response = $client->post('/2.0/sheets/'.$sheetId.'/rows', [
                'body' => $info
            ]);


            if ($response->getStatusCode() == 200) {
                return response()->json(['status' => true]);
            } else {
                return $response->getStatusCode();
            }

        } else {
            return response()->json(['error' => 'Sheet ID is empty']);
        }
    }

    //NEMOKAMA KONSULTACIJA

    public function consult(Request $request)
    {
        $consult_name = $request->input('consult_name');
        $consult_phone = $request->input('consult_phone');
        $consult_city = $request->input('consult_city');

        Mail::send('emails.consult',
            [
                'consult_name' => $consult_name,
                'consult_phone' => $consult_phone,
                'consult_city' => $consult_city,
            ],
            function ($message)
            {
                $message->from('admin@32balti.lt');
                $message->to('gytis@expertmedia.lt');
                $message->subject('Klausimas gydytojui - 32balti.lt');
            }
        );

        $this->consult_smartsheet($request->all());

        return response()->json(['status' => true]);

    }

    public function consult_smartsheet($data)
    {
        $consult_name = $data['consult_name'];
        $consult_phone = $data['consult_phone'];
        $consult_city = $data['consult_city'];

        $apiToken = 'gwny5rhgmv6mgjycycholxr53j';

        if (!empty($sheetId)) {
            $params = [
                'toTop' => true,
                'cells' => [
                    [
                        'columnId' => 1275035083138948,
                        'value' => $consult_name
                    ],
                    [
                        'columnId' => 3526834896824196,
                        'value' => $consult_phone
                    ],
                    [
                        'columnId' => 8030434524194692,
                        'value' => $consult_city
                    ],
                ]
            ];

            $info = json_encode($params);
            
            $client = new Client([
                'base_uri' => 'https://api.smartsheet.com',
                'timeout' => 2.0,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$apiToken
                ]
            ]);

            $response = $client->post('/2.0/sheets/'.$sheetId.'/rows', [
                'body' => $info
            ]);


            if ($response->getStatusCode() == 200) {
                return response()->json(['status' => true]);
            } else {
                return $response->getStatusCode();
            }

        } else {
            return response()->json(['error' => 'Sheet ID is empty']);
        }
    }
}
