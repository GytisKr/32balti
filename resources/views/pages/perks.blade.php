@extends('layouts.app')

@section('content')

<div id="why-us" class="inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.perks_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{ __('msg.whyus.title') }}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            @php
                $count = 0;
            @endphp
            @foreach ($perks as $perk)
                @php
                    $count++;
                @endphp
                <div class="column is-3">
                    <div class="perk-item">
                        <div class="perk-number">{{$count}}</div>
                        <div class="perk-title">{{$perk->title}}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection