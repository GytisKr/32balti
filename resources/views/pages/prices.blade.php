@extends('layouts.app')

@section('content')

<div id="prices" class="inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.prices_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{ __('msg.prices.title') }}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        @foreach ($prices as $price)
            <div class="price-block">
                <button class="accordion">{{$price->title}}<div class="plus"><span></span><span></span></div></button>
                <div class="panel">
                    <div class="price-body">
                        {!!$price->body!!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection