@extends('layouts.app')

@section('content')

<div id="contacts" class="inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.contacts_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{ __('msg.contacts.title') }}</h1>
            </div>
        </div>
    </div>
    <div class="footer-section section">
        <div class="container">
            {{-- <div class="columns">
                <div class="column is-12">
                    <div class="logo">
                        <a href="{{ URL::to('/') . '/' . app()->getLocale() }}">
                            <img src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.description') }}">
                        </a>
                    </div>
                </div>
            </div> --}}
            <div class="columns">
                <div class="column is-3">
                    <h3>Vilnius</h3>
                    <ul class="footer-list">
                    <li><a target="_blank" href="setting('site.map_link')">{{setting('site.adress')}}</a></li>
                        <li>M: <a href="{{setting('site.phone1')}}">{{setting('site.phone1')}}</a></li>
                        <li>T: <a href="{{setting('site.phone2')}}">{{setting('site.phone2')}}</a></li>
                        <li>E: <a href="{{setting('site.email')}}">{{setting('site.email')}}</a></li>
                    </ul>
                    <h3>{{ __('msg.footer.hours') }}</h3>
                    <ul class="footer-list">
                        <li>{{ __('msg.footer.weekdays') }}</li>
                        <li>{{setting('site.working_hours')}}</li>
                        <li>{{ __('msg.footer.weekdends') }}</li>
                    </ul>
                </div>
                <div class="column is-3">
                    <h3>{{ __('msg.footer.nav') }}</h3>
                    <ul class="footer-list">
                        {{ menu(app()->getLocale(), 'menus.home') }}
                    </ul>
                    <ul class="social-icons">
                        <li><a target="_blank" href="https://www.facebook.com/32Balti"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/32balti/?hl=en"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="column is-6">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2306.0004297203445!2d25.291751615784754!3d54.69202008028303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd95afef977571%3A0x5de52fb840a5dc94!2s32%20Balti!5e0!3m2!1sen!2slt!4v1606919865693!5m2!1sen!2slt" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="register">
        <div class="container">
            <h5 class="register-intro">{{ __('msg.register.intro') }}</h5>
            <div class="register-block">
                <h3>Registracija internetu</h3>
            <form id="registration-form" action="">
                <input type="hidden" name="sheet-id" value="5921454450599812">
            <div class="columns">
                <div class="column is-6">
                    <input required="" name="name" id="name" class="form-input" type="text" placeholder="Jūsų vardas">
                </div>
                <div class="column is-6">
                    <input required="" name="phone" id="phone" class="form-input" type="text" placeholder="Telefono numeris">
                </div>
            </div>
            <div class="columns">
                <div class="column is-4">
                    <select required="" class="form-input" name="time" id="form-time">
                        <option value="" selected="" disabled="" hidden="">Pasirinkite laiką</option>
                        <option value="8:00 - 12:00">8:00 - 12:00</option>
                        <option value="12:00 - 16:00">12:00 - 16:00</option>
                        <option value="16:00 - 20:00">16:00 - 20:00</option>
                    </select>
                </div>
                <div class="column is-4">
                    <select required="" class="form-input" name="purpose" id="form-goal">
                        <option value="" selected="" disabled="" hidden="">Vizito tikslas</option>
                        <option value="Esu naujas(-a) pacientas(-ė)">Esu naujas(-a) pacientas(-ė)</option>
                        <option value="Periodiškas patikrinimas">Periodiškas patikrinimas</option>
                        <option value="Konsultacija">Konsultacija</option>
                        <option value="Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)">Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)</option>
                    </select>
                </div>
                <div class="column is-4">
                    <select required="" class="form-input" name="service" id="form-service">
                        <option value="" selected="" disabled="" hidden="">Pasirinkite paslaugą</option>
                        <option value="Dantų plombavimas">Dantų plombavimas</option>
                        <option value="Profesionali burnos higiena">Profesionali burnos higiena</option>
                        <option value="Dantų balinimas">Dantų balinimas</option>
                        <option value="Dantų griežimo ir dilimo profilaktika">Dantų griežimo ir dilimo profilaktika</option>
                        <option value="Dantų implantacija">Dantų implantacija</option>
                        <option value="Dantų protezavimas">Dantų protezavimas</option>
                        <option value="Dantų laminatės">Dantų laminatės</option>
                        <option value="Estetinis plombavimas">Estetinis plombavimas</option>
                        <option value="Žandikaulio sąnario gydymas">Žandikaulio sąnario gydymas</option>
                        <option value="Nematomos tiesinimo kapos">Nematomos tiesinimo kapos</option>
                        <option value="Endodontinis gydymas">Endodontinis gydymas</option>
                        <option value="Periodontologija, dantenų recesijų gydymas">Periodontologija, dantenų recesijų gydymas</option>
                        <option value="Dantų šalinimas">Dantų šalinimas</option>
                        <option value="Burnos chirurgija">Burnos chirurgija</option>
                        <option value="Tyrimai ir diagnostika">Tyrimai ir diagnostika</option>
                        <option value="Sedacija">Sedacija</option>
                    </select>
                </div>
            </div>
            <textarea required="" class="form-input" name="fmessage" id="form-comment" cols="30" rows="5" placeholder="Papildoma informacija"></textarea>
            <button id="register-submit" type="submit" name="button" class="main-btn">Patvirtinti registraciją</button>
            <div class="success-message" style="display: none;">
                <h3>Registracija sėkminga!<br>Netrukus su jumis susisieksime!</h3>
            </div>
        </form>
    </div>
    </div>
</div>
</div>

@endsection