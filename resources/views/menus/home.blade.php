<div id="menu">
    @php
        if (Voyager::translatable($items)) {
            $items = $items->load('translations');
        }
    @endphp
    @foreach ($items as $item)
        @php
            $originalItem = $item;
            if (Voyager::translatable($item)) {
                $item = $item->translate($options->locale);
            }
            $isActive = null;
            // Check if link is current
            if(url($item->link()) == url()->current()){
                $isActive = 'active';
            }
        @endphp
        <div class="nav-item {{ $isActive }} @if(!$originalItem->children->isEmpty()) has-children @endif">
            @if (\Request::route()->getName() == 'homepage')
                <a href="{{ app()->getLocale() }}{{ $item->url }}">
            @else
                <a href="/{{ app()->getLocale() }}{{ $item->url }}">
            @endif
                <span>{{ $item->title }}</span>
            </a>
            @if(!$originalItem->children->isEmpty())
                <i class="fas fa-sort-down"></i>
            @endif
            @if(!$originalItem->children->isEmpty())
                {{-- @include('voyager::menu.default', ['items' => $originalItem->children, 'options' => $options]) --}}
                <ul class="dropdown-menu">
                    @foreach ($originalItem->children as $children)
                        <li>
                            @if (\Request::route()->getName() == 'homepage')
                                <a href="{{ app()->getLocale() }}{{ $children->url }}">
                            @else
                                <a href="{{ config('app.url') }}{{ app()->getLocale() }}{{ $children->url }}">
                            @endif
                                {{ $children->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    @endforeach
    @php
        $name = Route::currentRouteName();
        if ($name == 'pages.contacts-lt'):
    @endphp
    <a id="main-register" href="#register" class="contacts-register btn-register">{{ __("msg.form.register-online") }}</a>
    @php    
        else:
    @endphp
    <a id="main-register"class="toggle-register btn-register">{{ __("msg.form.register-online") }}</a>
    @php
        endif;
    @endphp

</div>

    