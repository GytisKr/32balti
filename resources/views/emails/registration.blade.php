<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>Registracija internetu - 32balti.lt</title>
</head>
<body>
    @if(!empty($name))
        <p>
            <strong>
                Vardas ir pavardė
            </strong>
            {{ $name }}
        </p>
    @endif
    @if(!empty($phone))
        <p>
            <strong>
                Telefono numeris
            </strong>
            {{ $phone }}
        </p>
    @endif
    @if(!empty($time))
        <p>
            <strong>
                Pasirinktas laikas
            </strong>
            {{ $time }}
        </p>
    @endif
    @if(!empty($purpose))
        <p>
            <strong>
                Vizito tikslas
            </strong>
            {{ $purpose }}
        </p>
    @endif
    @if(!empty($service))
        <p>
            <strong>
                Pasirinkita paslauga
            </strong>
            {{ $service }}
        </p>
    @endif
    @if(!empty($fmessage))
        <p>
            <strong>
                Papildoma informacija
            </strong>
            {{ $fmessage }}
        </p>
    @endif
</body>
</html>
