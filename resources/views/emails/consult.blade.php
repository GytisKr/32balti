<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>Nemokama konsultacija dėl dantų implantacijos - 32balti.lt</title>
</head>
<body>
    @if(!empty($consult_name))
        <p>
            <strong>
                Vardas
            </strong>
            {{ $consult_name }}
        </p>
    @endif
    @if(!empty($consult_phone))
        <p>
            <strong>
                Tel. numeris
            </strong>
            {{ $consult_phone }}
        </p>
    @endif
    @if(!empty($consult_city))
        <p>
            <strong>
                Miestas
            </strong>
            {{ $consult_city }}
        </p>
    @endif
</body>
</html>