<!DOCTYPE html>
<html lang="lt">
<head>
    <meta charset="UTF-8">
    <title>Parašykite gydytojui - 32balti.lt</title>
</head>
<body>
    @if(!empty($ask_doctor))
        <p>
            <strong>
                Gydytojas
            </strong>
            {{ $ask_doctor }}
        </p>
    @endif
    @if(!empty($ask_name))
        <p>
            <strong>
                Vardas ir pavardė
            </strong>
            {{ $ask_name }}
        </p>
    @endif
    @if(!empty($ask_email))
        <p>
            <strong>
                El. paštas
            </strong>
            {{ $ask_email }}
        </p>
    @endif
    @if(!empty($ask_phone))
        <p>
            <strong>
                Tel. numeris
            </strong>
            {{ $ask_phone }}
        </p>
    @endif
    @if(!empty($ask_comment))
        <p>
            <strong>
                Žinutė
            </strong>
            {{ $ask_comment }}
        </p>
    @endif
</body>
</html>