@extends('layouts.app')

@section('content')

<div class="service-inner inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.services_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{$service->title}}</h1>
            </div>
        </div>
    </div>
    <div class="service-body">
        <div class="container">
            <div class="back-btn">
                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}"><i class="fas fa-chevron-left"></i>{{ __('msg.global.back') }}</a>
            </div>
            <div class="body">
                {!!$service->body!!}
            </div>
            @php
                if ($service->duk!=null)
            @endphp
            <div class="duk">
                <h2>{{ __('msg.services.questions') }}</h2>
                {!!$service->duk!!}
            </div>
        </div>
    </div>
    <div class="container">   
        <div class="columns">
            <div class="column is-12">
                <div class="cta-btns">
                    <a class="toggle-register main-btn">{{ __('msg.register.title') }}</a><a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.contacts.url") }}" class="main-btn">{{ __('msg.contacts.title') }}</a>
                </div>
            </div>
        </div>
        @if ($service->doctors->count())
            <div id="doctors">
                <div class="columns">
                    <div class="column is-12">
                        <h2>{{ __("msg.doctors.recommended") }}</h2>
                    </div>
                </div>
                @php
                    $count = 0;
                @endphp
                @foreach ($service->doctors as $doctor)
                    @php
                        $count++;
                        if ($count<2):
                    @endphp
                    <div class="ask-question">
                        <div class="columns">
                            <div class="column is-3">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                    <div class="doctor-block">
                                        <div id="{{ $doctor->active }}-online" class="doctor-activity active">
                                            <p>{{ __("msg.doctors.online") }}</p>
                                        </div>
                                        <div id="{{ $doctor->active }}-offline" class="doctor-activity inactive">
                                            <p>{{ __("msg.doctors.offline") }}</p>
                                        </div>
                                        <img src="{{ Voyager::image($doctor->image) }}" alt="{{$doctor->title}}">
                                        <div class="content">
                                            <h3>{{$doctor->title}}</h3>
                                            <p>{{$doctor->position}}</p>
                                            <div class="plus">
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="column is-9">
                                <div class="form">
                                    <form id="ask-form" action="">
                                        <input type="hidden" name="sheet-id" value="5352617707628420">
                                        <div class="columns">
                                            <div class="column is-12">
                                                <input required="" name="ask_name" id="ask_name" class="form-input" type="text" placeholder="Jūsų vardas, pavardė">
                                            </div>
                                            <div class="column is-12">
                                                <input required="" name="ask_email" id="ask_email" class="form-input" type="text" placeholder="El. paštas">
                                            </div>
                                            <div class="column is-12">
                                                <input required="" name="ask_phone" id="ask_phone" class="form-input" type="text" placeholder="Tel. numeris">
                                                <input required="" name="ask_doctor" id="ask_doctor" class="form-input" type="hidden" value="{{$doctor->title}}">
                                            </div>
                                            <div class="column is-12">
                                                <textarea required="" class="form-input" name="ask_comment" id="ask_comment" cols="30" rows="10" placeholder="Jūsų klausimai mums?"></textarea>
                                                <div class="text-center">
                                                    <button id="register-submit" type="submit" name="button" class="main-btn">Siųsti</button>
                                                </div>
                                                <div class="success-message" style="display: none;">
                                                    <p>Žinutė išsiųsta<br>Netrukus su jumis susisieksime!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> 
                    @php
                        endif;
                    @endphp
                @endforeach
            </div>
        @endif
    </div>
</div>

@endsection