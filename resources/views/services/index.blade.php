@extends('layouts.app')

@section('content')

<div id="services" class="inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.services_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{ __('msg.services.title') }}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            @foreach ($services as $service)
                <div class="column is-3">
                    <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                        <div class="service-block">
                            <h3>{{$service->title}}</h3>
                            <img src="{{ Voyager::image($service->image) }}" alt="{{$service->title}}">
                            <div class="plus">
                                <span></span>
                                <span></span>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>
    </div>
</div>

@endsection