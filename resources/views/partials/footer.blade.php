    <footer>
        @php
            $name = Route::currentRouteName();
            if ($name != 'pages.contacts-lt'):
        @endphp
        <div class="footer-section section">
            <div class="container">
                <div class="columns">
                    <div class="column is-12">
                        <div class="logo">
                            <a href="{{ URL::to('/') . '/' . app()->getLocale() }}">
                                <img src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.description') }}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-3">
                        <h3>Vilnius</h3>
                        <ul class="footer-list">
                        <li><a target="_blank" href="setting('site.map_link')">{{setting('site.adress')}}</a></li>
                            <li>M: <a href="{{setting('site.phone1')}}">{{setting('site.phone1')}}</a></li>
                            <li>T: <a href="{{setting('site.phone2')}}">{{setting('site.phone2')}}</a></li>
                            <li>E: <a href="{{setting('site.email')}}">{{setting('site.email')}}</a></li>
                        </ul>
                        <h3>{{ __('msg.footer.hours') }}</h3>
                        <ul class="footer-list">
                            <li>{{ __('msg.footer.weekdays') }}</li>
                            <li>{{setting('site.working_hours')}}</li>
                            <li>{{ __('msg.footer.weekdends') }}</li>
                        </ul>
                    </div>
                    <div class="column is-3">
                        <h3>{{ __('msg.footer.nav') }}</h3>
                        <ul class="footer-list">
                            {{ menu(app()->getLocale(), 'menus.home') }}
                        </ul>
                        <ul class="social-icons">
                            <li><a target="_blank" href="https://www.facebook.com/32Balti"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/32balti/?hl=en"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="column is-6">
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2306.0004297203445!2d25.291751615784754!3d54.69202008028303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd95afef977571%3A0x5de52fb840a5dc94!2s32%20Balti!5e0!3m2!1sen!2slt!4v1606919865693!5m2!1sen!2slt" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
            endif;
        @endphp
        <div class="copyright">
            <div class="container">
                <p>© 2020 32:Balti</p>
            </div>
        </div>
    </footer>
</div> {{-- end #app --}}

<div style="display: none;" id="consult-box">
	<form id="consult-form" action="">
        <input type="hidden" name="sheet-id" value="3376888795162500">
        <div class="columns">
            <div class="column is-12">
                <input required="" name="consult_name" id="consult_name" class="form-input" type="text" placeholder="Jūsų vardas, pavardė">
            </div>
            <div class="column is-12">
                <input required="" name="consult_phone" id="consult_phone" class="form-input" type="text" placeholder="Tel. numeris">
                <input required="" name="consult_city" id="consult_city" class="form-input" type="hidden" value="Vilnius">
            </div>
            <div class="column is-12">
                <div class="text-center">
                    <button id="register-submit" type="submit" name="button" class="ask_submit main-btn">Registruotis</button>
                </div>
                <div class="success-message" style="display: none;">
                    <p>Registracija sėkminga<br>Netrukus su jumis susisieksime!</p>
                </div>
            </div>
        </div>
    </form>
</div>



<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>