<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ Voyager::image(setting('site.favicon')) }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;600;900&display=swap" rel="stylesheet">
    <title>
        @hasSection ('meta-title')
            @yield('meta-title') - {{ setting('site.title') }} {{ setting('site.description') }}
        @else
            {{ setting('site.title') }} {{ " | " }} {{ setting('site.description') }}
        @endif
    </title>
    <meta name="description" content="@hasSection('meta-description') @yield('meta-description') @else {{ setting('site.description') }} @endif">

    @if (!empty(setting('header-footer.header')))
        {!! setting('header-footer.header') !!}
    @endif
</head>