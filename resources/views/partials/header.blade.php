<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.head')

    @php
        $languages = config('voyager.multilingual.locales');
        $navclass = '';
        $name = Route::currentRouteName();
        if ($name == 'doctors.show-lt' || $name == 'services.show-lt')
        $navclass = 'nav-active'
    @endphp
<body>
    <div id="app">
        <header>
            <div class="nav-back @php echo $navclass; @endphp">
                <div class="container">
                    <div class="navigation @php echo $navclass; @endphp">
                        <div class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div class="top-navigation">
                            <div class="languages">
                                @foreach ($languages as $language)
                                <div class="lang-item">
                                    <a href="{{ URL::to('/') }}/{{ $language }}">{{ $language }}</a>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="logo">
                            <a href="{{ URL::to('/') . '/' . app()->getLocale() }}">
                                <img src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.description') }}">
                            </a>
                        </div>
                        <div id="nav">
                            {{ menu(app()->getLocale(), 'menus.home') }}
                        </div>
                    </div>
                </div>
            </div>
        </header>
        @php
            if ($name != 'pages.contacts-lt'):
        @endphp
        <div id="register" class="">
            <div class="register-block">
                <a class="close-reg-form toggle-register" href="javascript:;">×</a>
                <h3>{{ __('msg.register.title') }}</h3>
                <form id="registration-form" action="">
                <input type="hidden" name="sheet-id" value="5921454450599812">
                <div class="columns">
                    <div class="column is-6">
                        <input required="" name="name" id="name" class="form-input" type="text" placeholder="Jūsų vardas">
                    </div>
                    <div class="column is-6">
                        <input required="" name="phone" id="phone" class="form-input" type="text" placeholder="Telefono numeris">
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-4">
                        <select required="" class="form-input" name="time" id="form-time">
                            <option value="" selected="" disabled="" hidden="">Pasirinkite laiką</option>
                            <option value="8:00 - 12:00">8:00 - 12:00</option>
                            <option value="12:00 - 16:00">12:00 - 16:00</option>
                            <option value="16:00 - 20:00">16:00 - 20:00</option>
                        </select>
                    </div>
                    <div class="column is-4">
                        <select required="" class="form-input" name="purpose" id="form-goal">
                            <option value="" selected="" disabled="" hidden="">Vizito tikslas</option>
                            <option value="Esu naujas(-a) pacientas(-ė)">Esu naujas(-a) pacientas(-ė)</option>
                            <option value="Periodiškas patikrinimas">Periodiškas patikrinimas</option>
                            <option value="Konsultacija">Konsultacija</option>
                            <option value="Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)">Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)</option>
                        </select>
                    </div>
                    <div class="column is-4">
                        <select required="" class="form-input" name="service" id="form-service">
                            <option value="" selected="" disabled="" hidden="">Pasirinkite paslaugą</option>
                            <option value="Dantų plombavimas">Dantų plombavimas</option>
                            <option value="Profesionali burnos higiena">Profesionali burnos higiena</option>
                            <option value="Dantų balinimas">Dantų balinimas</option>
                            <option value="Dantų griežimo ir dilimo profilaktika">Dantų griežimo ir dilimo profilaktika</option>
                            <option value="Dantų implantacija">Dantų implantacija</option>
                            <option value="Dantų protezavimas">Dantų protezavimas</option>
                            <option value="Dantų laminatės">Dantų laminatės</option>
                            <option value="Estetinis plombavimas">Estetinis plombavimas</option>
                            <option value="Žandikaulio sąnario gydymas">Žandikaulio sąnario gydymas</option>
                            <option value="Nematomos tiesinimo kapos">Nematomos tiesinimo kapos</option>
                            <option value="Endodontinis gydymas">Endodontinis gydymas</option>
                            <option value="Periodontologija, dantenų recesijų gydymas">Periodontologija, dantenų recesijų gydymas</option>
                            <option value="Dantų šalinimas">Dantų šalinimas</option>
                            <option value="Burnos chirurgija">Burnos chirurgija</option>
                            <option value="Tyrimai ir diagnostika">Tyrimai ir diagnostika</option>
                            <option value="Sedacija">Sedacija</option>
                        </select>
                    </div>
                </div>
                <textarea required="" class="form-input" name="fmessage" id="form-comment" cols="30" rows="5" placeholder="Papildoma informacija"></textarea>
                <button id="register-submit" type="submit" name="button" class="main-btn">{{ __("msg.register.approve") }}</button>
                  <div class="success-message" style="display: none;">
                    <h3>Registracija sėkminga!<br>Netrukus su jumis susisieksime!</h3>
                </div>
                </form>
            </div>
        </div>
        @php
            endif;
        @endphp