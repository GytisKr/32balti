@extends('layouts.app')

@section('content')

<div id="doctors" class="inner-page section">
    <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.doctors_cover')) }}')">
        <div class="container">
            <div class="cover-content">
                <h1>{{ __('msg.doctors.title') }}</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="columns">
            @foreach ($doctors as $doctor)
                <div class="column is-4">
                    <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                        <div class="doctor-block">
                            <img src="{{ Voyager::image($doctor->image) }}" alt="{{$doctor->title}}">
                            <div class="content">
                                <h3>{{$doctor->title}}</h3>
                                <p>{{$doctor->position}}</p>
                                <div class="plus">
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection