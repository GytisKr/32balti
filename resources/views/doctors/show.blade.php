@extends('layouts.app')

@section('content')

    <div class="doctor-inner">
        <div class="container">
            <div class="doctor-intro">
                <div class="columns">
                    <div class="column is-6">
                        <div class="doctor-image" style="background-image:url('{{ Voyager::image($doctor->image) }}')">
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="doctor-info">
                            <h2>{{$doctor->title}}</h2>
                            <h5>{{$doctor->position}}</h5>
                            <div class="about-docotr">
                                <p>
                                    {{$doctor->excerpt}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-12">
                        <div class="cta-btns">
                            <a class="toggle-register main-btn">{{ __('msg.register.title') }}</a><a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.contacts.url") }}" class="main-btn">{{ __('msg.contacts.title') }}</a>
                        </div>
                        <div class="doctor-body">
                            <div class="info">
                                {!!$doctor->body!!}
                            </div>
                            @php
                                if ($doctor->qualification != null):
                            @endphp
                            <div class="qualification">
                                <div class="qualification-toggle plus">
                                    <span></span>
                                    <span></span>
                                </div>
                                <h2>{{ __('msg.doctors.qualification') }}</h2>
                                <div class="qualification-list">
                                    {!!$doctor->qualification!!}
                                </div>
                            </div>
                            @php
                                endif;
                                $images = json_decode($doctor->works);
                                if ($images != null):
                            @endphp
                            <div class="works">
                                <h2>{{ __('msg.doctors.results') }}</h2>
                                <div class="columns">
                                    @foreach ($images as $image)
                                    <div class="column is-6">
                                        <a href="{{ Voyager::image($image) }}" data-fancybox="images" data-caption="My caption">
                                            <img src="{{ Voyager::image($image) }}" alt="" />
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @php
                                endif;
                            @endphp
                            @if ($doctor->services->count())
                                <div id="services">
                                    <div class="columns">
                                        <div class="column is-12">
                                            <h2>{{ __("msg.services.recommended") }}</h2>
                                        </div>
                                    </div>
                                    <div class="columns">
                                        @foreach ($doctor->services as $service)
                                            <div class="column is-3">
                                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                                                    <div class="service-block">
                                                        <h3>{{$service->title}}</h3>
                                                        <img src="{{ Voyager::image($service->image) }}" alt="{{$service->title}}">
                                                        <div class="plus">
                                                            <span></span>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="cta-btns">
                            <a class="toggle-register main-btn">{{ __('msg.register.title') }}</a><a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.contacts.url") }}" class="main-btn">{{ __('msg.contacts.title') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection