@extends('layouts.app')

@section('content')

<div class="app">
    {{-- Slider --}}
    <div id="sliders">
        @foreach ($sliders as $slider)
        <div class="slider-item" style="background-image: url('{{ Voyager::image($slider->image) }}')">
            <div class="slider-content">
                <h1>{{ $slider->title }}</h1>
                <h2>{{ $slider->subtitle }}</h2>
            </div>
        </div>
        @endforeach
        <div class="container">
            <div class="highlighted-services">
                @foreach ($highlights as $highlight)
                <div class="service-item">
                    <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $highlight->slug }}">
                        <h3>{{$highlight->title}}</h3>
                        <div class="read-more">
                            {{ __('msg.services.more') }}<i class="fas fa-chevron-right"></i>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    {{-- Whyus --}}
    <div id="why-us" class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">{{ __('msg.whyus.title') }}</h1>
                </div>
            </div>
            <div class="columns">
                @php
                    $count = 0;
                @endphp
                @foreach ($perks as $perk)
                    @php
                        $count++;
                    @endphp
                    <div class="column is-3">
                        <div class="perk-item">
                            <div class="perk-number">{{$count}}</div>
                            <div class="perk-title">{{$perk->title}}</div>
                        </div>
                    </div>
                @endforeach
                <div class="column is-12 text-center">
                    <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.whyus.url") }}" class="main-btn">{{ __('msg.whyus.showall') }}</a>
                </div>
            </div>
        </div>
    </div>
    {{-- Doctors --}}
    <div id="doctors" class="section">
        <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.doctors_cover')) }}')">
            <div class="container">
                <div class="cover-content">
                    <h1>{{ __('msg.doctors.title') }}</h1>
                    <p>{{ __('msg.doctors.intro') }}</p>
                    <a class="toggle-register main-btn white-btn" href="">{{ __('msg.form.reserve') }}</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">{{ __('msg.doctors.title') }}</h1>
                </div>
            </div>
            <div class="columns doctor-carousel">
                @foreach ($doctors as $doctor)
                    <div class="column is-3">
                        <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                            <div class="doctor-block">
                                <img src="{{ Voyager::image($doctor->image) }}" alt="{{$doctor->title}}">
                                <div class="content">
                                    <h3>{{$doctor->title}}</h3>
                                    <p>{{$doctor->position}}</p>
                                    <div class="plus">
                                        <span></span>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <div class="columns">
                <div class="column is-12 text-center">
                    <a class="main-btn" href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}">{{ __('msg.doctors.all') }}</a>
                </div>
            </div>
        </div>
    </div>
    {{-- Services --}}
    <div id="services" class="section">
        <div class="section-cover" style="background-image: url('{{ Voyager::image(setting('covers.services_cover')) }}')">
            <div class="container">
                <div class="cover-content">
                    <h1>{{ __('msg.services.title') }}</h1>
                    <p>{{ __('msg.services.intro') }}</p>
                    <a class="main-btn white-btn" href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.prices.url") }}">{{ __('msg.services.pricing') }}</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="columns">
                @foreach ($services as $service)
                    <div class="column is-3">
                        <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                            <div class="service-block">
                                <h3>{{$service->title}}</h3>
                                <img src="{{ Voyager::image($service->image) }}" alt="{{$service->title}}">
                                <div class="plus">
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
                <div class="column is-12 text-center">
                    <a class="main-btn" href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}">{{ __('msg.services.all') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div id="aboutus" class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-12">
                    <div class="aboutus-intro">
                        <h1 class="section-title">{{ __('msg.aboutus.title') }}</h1>
                        <p>{{ __('msg.aboutus.intro') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection