<?php

return [
  'global' => [
    'back' => 'Grįžti',
  ],
  'form' => [
    'register-online' => 'Registracija internetu',
    'reserve' => 'Rezervuoti laiką',
  ],
  'whyus' => [
    'title' => '32 kodėl mes?',
    'showall' => 'matyti visus',
    'url' => 'kodel-mes',
  ],
  'doctors' => [
    'title' => 'Gydytojai',
    'intro' => 'Nuolatinis profesinis tobulėjimas bei šiuolaikinių technologijų taikymas, šią komandą daro tinkama kiekvienam.',
    'all' => 'Visi gydytojai',
    'url' => 'gydytojai',
    'qualification' => 'Kvalifikacijos tobulinimas',
    'results' => 'Rezultatai',
    'recommended' => 'Užduokite gydytojui klausimą dėl šios procedūros',
    'online' => 'Prisijungęs',
    'offline' => 'Neprisijungęs',
  ],
  'services' => [
    'title' => 'Paslaugos',
    'intro' => 'Ištyrimas, konsultavimas, gydymo planavimas, jo pristatymas pacientui bei gydymas. Visos šiuolaikinės odontologijos procedūros vienoje vietoje.',
    'all' => 'Visos paslaugos',
    'pricing' => 'Kainoraštis',
    'url' => 'paslaugos',
    'recommended' => 'Gydytojas atlieka šias procedūras:',
    'more' => 'Plačiau',
    'questions' => 'Turite klausimų? Atsakome!',
  ],
  'aboutus' => [
    'title' => 'Apie mus',
    'intro' => 'Tai moderni odontologijos klinika galinti išpildyti kiekvieno norus. Tai savo darbą puikiai išmanančių, nuolat tobulėjančių, o svarbiausia jį mylinčių žmonių komanda.',
  ],
  'footer' => [
    'hours' => 'Darbo laikas',
    'weekdays' => 'Pirmadienis - penktadienis',
    'weekdends' => 'Savaitgaliais NEDIRBAME',
    'nav' => 'Greitoji navigacija',
  ],
  'contacts' => [
    'title' => 'Kontaktai',
    'url' => 'kontaktai',
  ],
  'prices' => [
    'title' => 'Kainos',
    'url' => 'kainos',
  ],
  'register' => [
    'title' => 'Registracija internetu',
    'approve' => 'Patvirtinti registraciją',
    'intro' => 'Mūsų klinikoje galite užsiregistruoti ne darbo valandomis ir savaitgaliais',
  ],
];