const { toArray } = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
    window.fancy = require('@fancyapps/fancybox');
    window.slick = require('slick-carousel');
} catch (e) {}

/*
Doctor Slider
*/

$('.doctor-carousel').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
        }
      },
    ]
});

/*
Price accordion
*/ 

var acc = document.getElementsByClassName("accordion");
var pan = document.getElementsByClassName("panel");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    panel.classList.toggle("panel-active");
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

/**
 * Main register toggle
 */

$('.toggle-register').on('click', function(e) {
  $('#register').toggleClass("register-active"); //you can list several class names 
  e.preventDefault();
});

$('.service-body h4').on('click', function(e) {
  $('#register').toggleClass("register-active"); //you can list several class names 
  e.preventDefault();
});

/**
 * Qualification toggle
 */

$('.qualification-list').on('click', function(e) {
  $('.qualification-list').toggleClass("list-active"); //you can list several class names 
  e.preventDefault();
});

/**
 * Main menu toggle
 */

$('.hamburger').on('click', function(e) {
    $('#nav').toggleClass("menu-is-open");
    $('.hamburger').toggleClass("x-hamburger"); //you can list several class names 
    e.preventDefault();
});

/**
 * Dropdown menu toggle
 */

$('.fa-sort-down').on('click', function(e) {
  $('.dropdown-menu').toggleClass("dropdown-menu-active"); //you can list several class names 
  e.preventDefault();
});

/*
 * Doctor activity
 */

var d = new Date();
var n = d.getHours();
var w = d.getDay();

// Morning
var morningOn = document.getElementById("morning-online");
var morningOff = document.getElementById("morning-offline");

if (morningOn != null && morningOff != null) {
    if (n >= 7 && n <=21 && w != 0 && w != 6) {
        morningOn.classList.add("is-on");
    }
    else {
        morningOff.classList.add("is-on");
    }
}

// Afternoon
// var afternoonOn = document.getElementById("afternoon-online");
// var afternoonOff = document.getElementById("afternoon-offline");

// if (afternoonOn != null && afternoonOff != null) {
//     if (n >= 12 && n <=16 && w != 0 && w != 6) {
//         afternoonOn.classList.add("is-on");
//     }
//     else {
//         afternoonOff.classList.add("is-on");
//     }
// }

// // Evening
// var eveningOn = document.getElementById("evening-online");
// var eveningOff = document.getElementById("evening-offline");

// if (eveningOn != null && eveningOff != null) {
//     if (n >= 16 && n <=20 && w != 0 && w != 6) {
//         eveningOn.classList.add("is-on");
//     }
//     else {
//         eveningOff.classList.add("is-on");
//     }
// }

/*
Smooth scrolling
*/

$('a[href*="#"]')
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) {
            return false;
          } else {
            $target.attr('tabindex','-1');
            $target.focus();
          };
        });
      }
    }
});

/*
* Register forms
*/

/**
 * Forma
 */

$('#registration-form').on('submit', function(e) {
  e.preventDefault();

  let form = $(this);
  let formData = form.serialize();
  let successMsg = form.find('.success-message');


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: window.location.origin + '/lt/forma',
    method: 'POST',
    data: formData,
    success: function(res) {

      if (res.status) {
        successMsg.show();

        setTimeout(() => {
          successMsg.hide();
          form[0].reset();
        }, 4000);
      }
    },
    error: function(err) {
      console.log(err);
    }
  });

});

$('#ask-form').on('submit', function(e) {
  e.preventDefault();

  let form = $(this);
  let formData = form.serialize();
  let successMsg = form.find('.success-message');


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: window.location.origin + '/lt/susisiek',
    method: 'POST',
    data: formData,
    success: function(res) {

      if (res.status) {
        successMsg.show();

        setTimeout(() => {
          successMsg.hide();
          form[0].reset();
        }, 4000);
      }
    },
    error: function(err) {
      console.log(err);
    }
  });

});


$('#consult').on('submit', function(e) {
  e.preventDefault();

  let form = $(this);
  let formData = form.serialize();
  let successMsg = form.find('.success-message');


  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: window.location.origin + '/lt/konsultacija',
    method: 'POST',
    data: formData,
    success: function(res) {

      if (res.status) {
        successMsg.show();

        setTimeout(() => {
          successMsg.hide();
          form[0].reset();
        }, 4000);
      }
    },
    error: function(err) {
      console.log(err);
    }
  });

});
