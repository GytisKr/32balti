<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;


Route::get('/', 'HomeController@redirect');


Route::group(['prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 'middleware' => 'setLocale'], function () {
    
    Route::get('/', 'HomeController@index')->name('homepage');

    Route::get('gydytojai', 'DoctorsController@index')->name('doctors.index-' . app()->getLocale());
    Route::get('gydytojai/{slug}', 'DoctorsController@show')->name('doctors.show-' . app()->getLocale());
    
    Route::get('paslaugos', 'ServicesController@index')->name('services.index-' . app()->getLocale());
    Route::get('paslaugos/{slug}', 'ServicesController@show')->name('services.show-' . app()->getLocale());

    // Route::get('informatyvu', 'ArticlesController@index')->name('articles.index-' . app()->getLocale());
    // Route::get('informatyvu/{slug}', 'ArticlesController@show')->name('articles.show-' . app()->getLocale());
    
    /**
     * Register
     */

    Route::post('forma', 'ContactsController@register')->name('contacts.register');
    Route::post('susisiek', 'ContactsController@contactus')->name('contacts.contactus');
    Route::post('konsultacija', 'ContactsController@consult')->name('contacts.consult');

    /**
     * Pages
     */
    // Route::get('{slug}', 'PagesController@show');
    Route::get('kainos', 'PricesController@index')->name('pages.prices-' . app()->getLocale());
    Route::get('kontaktai', 'ContactsController@index')->name('pages.contacts-' . app()->getLocale());
    Route::get('kodel-mes', 'PerksController@index')->name('pages.perks-' . app()->getLocale());

});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
